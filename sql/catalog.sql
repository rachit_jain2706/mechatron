-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 10, 2016 at 05:34 AM
-- Server version: 5.7.9
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quizdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `catalog`
--

DROP TABLE IF EXISTS `catalog`;
CREATE TABLE IF NOT EXISTS `catalog` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `image` varchar(1000) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `price` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `catalog`
--

INSERT INTO `catalog` (`id`, `image`, `name`, `price`) VALUES
(1, 'uploads/catalog/chasis.jpg', 'chasis', 4000),
(2, 'uploads/catalog/battery.jpg', 'Battery', 2500),
(3, 'uploads/catalog/steering.jpg', 'Steering', 1500),
(4, 'uploads/catalog/Pistons3.jpg', 'piston', 2000),
(5, 'uploads/catalog/gear_assembly.jpg', 'Gear assembly', 2500),
(6, 'uploads/catalog/clutch.jpg', 'clutch', 1000),
(7, 'uploads/catalog/car-brakes.jpg', 'Brakes', 2500),
(8, 'uploads/catalog/coolant.jpg', 'coolant', 2000),
(9, 'uploads/catalog/suspension.jpg', 'suspension(single)', 500),
(10, 'uploads/catalog/fuel_injector.jpg', 'fuel injectors', 1000),
(11, 'uploads/catalog/axle.jpeg', 'Axle', 1000),
(12, 'uploads/catalog/tyres.jpg', 'Tyres', 2000),
(13, 'uploads/catalog/air-conditioning.jpg', 'Air conditioning', 2500),
(14, 'uploads/catalog/radiator.jpg', 'Radiator', 1000),
(15, 'uploads/catalog/compressor.jpg', 'AC Compressor', 500),
(16, 'uploads/catalog/engine_fan.jpg', 'Engine fan', 750),
(17, 'uploads/catalog/transmission.jpg', 'Transmission', 2000),
(18, 'uploads/catalog/shock_absorbers.jpg', 'Shock absorbers', 500),
(19, 'uploads/catalog/spark_plug.jpg', 'Spark plug', 500),
(20, 'uploads/catalog/air_filter.jpg', 'Air filter', 1000),
(21, 'uploads/catalog/cat_conv.jpg', 'catalystic converter', 1500),
(22, 'uploads/catalog/alternator.jpg', 'Alternator', 500),
(23, 'uploads/catalog/Pressure-Gauge.jpg', 'Pressure Gauge', 500),
(24, 'uploads/catalog/steering.jpg', 'Steering', 1000),
(25, 'uploads/catalog/exhaust.jpg', 'Exhaust system', 2500),
(26, 'uploads/catalog/wipers.jpg', 'Wipers', 500),
(27, 'uploads/catalog/lights.jpg', 'Lights', 1000),
(28, 'uploads/catalog/bumper.jpg', 'Bumper', 2000),
(29, 'uploads/catalog/seats.jpg', 'Seats', 2500),
(30, 'uploads/catalog/engine.jpg', 'Engine', 4500),
(31, 'uploads/catalog/bonnet.jpg', 'Bonnet', 1500),
(32, 'uploads/catalog/crankshaft.jpg', 'Crank shaft', 1200),
(33, 'uploads/catalog/carburetor.jpg', 'Carburetor', 1500),
(34, 'uploads/catalog/bearings.jpg', 'Bearings', 500),
(35, 'uploads/catalog/paint.jpg', 'Paint', 2000),
(36, 'uploads/catalog/dash_board.jpg', 'Dash board', 1000),
(37, 'uploads/catalog/sound_sys.jpg', 'Sound system', 500),
(38, 'uploads/catalog/horn.jpg', 'Horn', 1000),
(39, 'uploads/catalog/doors.jpg', 'Front doors', 1000),
(40, 'uploads/catalog/doors.jpg', 'Rear doors', 1000),
(41, 'uploads/catalog/speedometer.jpg', 'Speedometer', 500),
(42, 'uploads/catalog/brake_fluid.jpg', 'Brake fluid', 1000),
(43, 'uploads/catalog/clutch_fluid.jpg', 'Clutch fluid', 1000),
(44, 'uploads/catalog/timing_belt.jpg', 'Timing belt', 2000),
(45, 'uploads/catalog/gear_assembly.jpg', 'Gears', 2500),
(46, 'uploads/catalog/differential.jpg', 'Differential', 1000);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
