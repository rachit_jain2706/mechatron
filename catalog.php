<!DOCTYPE html>
<html lang="en">
<head>
  <title>Mechatron | TechTatva'16</title>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/styles.css">

   <!-- JS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <script type="text/javascript" src="js/common.js"></script>

  <script>

    function startTimer(duration, display_min, display_sec) 
    {
      var timer = duration, minutes, seconds;     
      setInterval(function () 
      {
          minutes = parseInt(timer / 60, 10)
          seconds = parseInt(timer % 60, 10);

          minutes = minutes < 10 ? "0" + minutes : minutes;
          seconds = seconds < 10 ? "0" + seconds : seconds;

          display_min.textContent = minutes;
          display_sec.textContent = seconds;

          if (--timer < 0) {
              window.open("start_quiz.php","_self");
          }
      }, 1000);
    } 

    window.onload = function() 
    {
      var fiveMinutes = 60 * 5;
      var display_min = document.querySelector("#minute");
      var display_sec = document.querySelector("#second");
      startTimer(fiveMinutes, display_min, display_sec);
    }

  </script>
</head>
<body>

  <!-- Navigation Page
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->    
  
  <?php
    require_once("includes/header.php");
    require_once("dbcon.php");
    session_start();
    if(isset($_POST["submit"]))
    {
      $t = time();
      $query="INSERT INTO login VALUES (".$_SESSION["userid"].", ".$t.")";
      mysqli_query($connection,$query);
      $link = "<script>window.open('start_quiz.php','_self')</script>";
      echo $link;

echo $link;
    }

    echo '<form action="" method="post">';
    echo '<div id="clockdiv" style="margin-left: 87%;">
            <div>
              <span class="minutes" id="minute">05</span>
              <div class="smalltext">Minutes</div>
            </div>
            <div>
              <span class="seconds" id="second">00</span>
              <div class="smalltext">Seconds</div>
            </div>
            <input type="submit" name="submit" value="Start Now">
          </div>
          </form>';

    $query = "SELECT * FROM catalog";
    $result = mysqli_query($connection, $query);

    if($result)
    {
      echo "<ul class='products'>";
      while($row = mysqli_fetch_assoc($result))
      {
        echo "<li>
                <img src={$row['image']} style='width: 200px; height:200px'>
                <h4><strong>Name : </strong>".$row["name"]."</h4>
                <h4><strong>Price : </strong>".$row["price"]."</h4>
              </li>";
      }
      echo "</ul>";
    }
  ?>

<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>