<!DOCTYPE html>
<html lang="en">
<head>
  <title>Mechatron | TechTatva'16</title>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/styles.css">

  <script type="text/javascript" src="js/common.js"></script>

  <script>
    function seeCatalog()
    {
      window.open("catalog.php","_self");
      //window.open("catalog.php","_self","location=no,menubar=no,resizable=no,toolbar=no");
      //window.open("catalog.php", "_self", "toolbar=no,scrollbars=no,resizable=no,location=no,fullscreen=yes");
    }
  </script>
  

</head>
<body>

  <!-- Navigation Page
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->  
  
  <?php
    require_once("includes/header.php"); 
  ?>
  
<body>

  <div class="instruction_div">
    <div class="login_heading">
      <h2>Instructions</h2>
    </div>
    <ol>
      <li>
        There are 175 MCQ questions.
      </li>
      <li>
        With every correct answer, the contestant will be awardeded Rs.500.
      </li>      
      <li>
        With enough money, the required components can be bought for the car to function.
      </li>
      <li>
        The first 5 minutes of the game will be for the contestant to see the catalog. The catalog cannot be viewed before
        the game ends.         
      </li>
      <li>
        Once the 5 minutes are over, the quiz will begin. The quiz will be for 1 hour.
      </li>
      <li>
        After the quiz is over, the contestant gets 5 minutes to select the items to make a working model of a car.
      </li>
      <li>
        <strong>Do not refresh the page.</strong> Refreshing the page will NOT restart the timer but will reset all the saved answers.
      </li>
      <li>
        <strong>A working model will hold more value as compared to an aesthetically pleasing model.</strong>
      </li>
    </ol>
  </div>
  <div id="start_button">
    <button type="button" class="start" onclick="seeCatalog()">Start</button>
  </div>

<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>
