function goPrev(id)
{
  var ID = document.getElementById("question_" + id);
  ID.removeAttribute('class');
  ID.setAttribute('class', 'hide');
  var newId = document.getElementById("question_"+ (id - 1));
  newId.setAttribute('class', 'question_div');      
}

function goNext(id)
{
  var ID = document.getElementById("question_" + id);      
  ID.setAttribute('class', 'hide');
  var newId = document.getElementById("question_"+ (id + 1));
  newId.setAttribute('class', 'question_div');
}

function sendRequest(target, json, responsecallBack) 
{
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function ()
  {
      if (xhttp.readyState == 4 && xhttp.status == 200) 
      {           
          var json = JSON.parse(xhttp.responseText);
          if(responsecallBack != null)
          {
          	responsecallBack(json.json);
         	}
      }
  };
  xhttp.open("POST", target, true);
  xhttp.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
  xhttp.send(json);
}

window.onkeydown = function(e)
{
  if( (e.which || e.keyCode) == 116 || (e.which || e.keyCode) == 8 || (e.which || e.keyCode)== 122 || (e.which || e.keyCode) == 27)
    e.preventDefault(); 
}

window.onkeypress = function(e)
{
  if( (e.which || e.keyCode) == 85 || (e.which || e.keyCode) == 73 )
    e.preventDefault(); 
}

document.addEventListener("contextmenu", function(e){
    e.preventDefault();
}, false);