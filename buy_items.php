<!DOCTYPE html>
<html lang="en">
<head>
  <title>Mechatron | TechTatva'16</title>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/styles.css">

   <!-- JS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
   <script type="text/javascript" src="js/common.js"></script>

   <script>

    function startTimer(duration, display_min, display_sec) 
    {
      var timer = duration, minutes, seconds;     
      setInterval(function () 
      {
          minutes = parseInt(timer / 60, 10)
          seconds = parseInt(timer % 60, 10);

          minutes = minutes < 10 ? "0" + minutes : minutes;
          seconds = seconds < 10 ? "0" + seconds : seconds;

          display_min.textContent = minutes;
          display_sec.textContent = seconds;

          if (--timer < 0) {
              storeCart();
          }
      }, 1000);
    }

    window.onload = function() 
    {
      var fiveMinutes = 60 * 5;
      var display_min = document.querySelector("#minute");
      var display_sec = document.querySelector("#second");
      startTimer(fiveMinutes, display_min, display_sec);
    }

    function changeMoney(id, price)
    {
      var item = document.getElementById(id);
      var money = parseInt(document.getElementById("money_value").textContent);      
      if(item.style.backgroundColor == "rgb(0, 188, 212)")
      {
        item.style.backgroundColor = "#FFFFFF";
        money += price;
        document.getElementById("money_value").textContent = money;
      }
      else
      {
        if(money < 0 || money < price)
        {
          alert("You do not have that much money. Please maintain aukat.");
        }
        else
        {
          item.style.backgroundColor = "#00BCD4";
          money -= price;
          document.getElementById("money_value").textContent = money;
        } 
      }
    }

    function storeCart()
    {
      var i = 0, k = 0;
      var cart = new Array();
      var carts = document.getElementById("cart").textContent;
      var money = parseInt(document.getElementById("money_value").textContent);
      for(i = 0; i < carts; i++)
      {
        var item = document.getElementById("que_" + i);
        if(item.style.backgroundColor == "rgb(0, 188, 212)")
        {
          cart[k++] = i + 1;
        }
      }
      sendRequest("includes/store_result.php", JSON.stringify(cart), responseCallBack());
    }

    function responseCallBack()
    {
      window.open("end_quiz.php","_self");
    }

  </script>
  

</head>
<body>

  <!-- Navigation Page
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->    
  
  <?php
    require_once("includes/header.php");
    require_once("dbcon.php");
    session_start();
    
    $j = 0; 
    $money = 0;
    if(isset($_POST["submit"]))
    {
      for($j = 0; $j <= 177; $j++)
      {
        if(isset($_POST["radio".$j]))
        {
          $optionSelected = $_POST['radio'.$j];
          if(strcmp($optionSelected, $_SESSION['answer'][$j]) == 0)
          {
            $money = $money + 500;                      
          }              
        }
      }
    }

    echo "<center><h1 style='display:inline;'>You have Rs.</h1>";
    echo "<h1 style='display:inline;' id='money_value'>".$money."</h1></center>";

    echo '<div id="clockdiv" style="margin-left: 87%;">
            <div>
              <span class="minutes" id="minute">05</span>
              <div class="smalltext">Minutes</div>
            </div>
            <div>
              <span class="seconds" id="second">00</span>
              <div class="smalltext">Seconds</div>
            </div>
            <button onclick="storeCart()">Submit Cart</button>
          </div>';

    $query = "SELECT * FROM catalog";
    $result = mysqli_query($connection, $query);

    if($result)
    {
      echo "<ul class='products'>";
      $i = 0;
      while($row = mysqli_fetch_assoc($result))
      {
        echo "<li id='".$row['id']."'>
                  <button class='item_button' id='que_".$i."' onclick='changeMoney(id, ".$row["price"].")'>
                    <img src={$row['image']} style='width: 200px; height:200px'>
                    <h4><strong>Name : </strong>".$row["name"]."</h4>
                    <h4><strong>Price : </strong>".$row["price"]."</h4>
                  </button>
              </li>";
        $i++;
      }
      echo "</ul>";
      echo "<p style='display:none' id='cart'>".$i."</p>";
    }
  ?>

<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>