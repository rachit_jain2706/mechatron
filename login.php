<!DOCTYPE html>
<html lang="en">
<head>
  <title>Mechatron | TechTatva'16</title>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/styles.css">

</head>
<body>

  <!-- Navigation Page
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->  
  
  <?php
    require_once("includes/header.php");    
  ?>
  <?php
    if(isset($_POST["submit"]))
    {
      session_start();
      $_SESSION["userid"]=$_POST["username"];
      if($_POST["password"] === "L_Dies")
      {
        header("Location:instruction_page.php");
      }
      else
      {
        echo "<script>alert('L DOES die.')</script>";
      }
    }
  ?>
  
<body background="uploads/b3.jpg">

  <div>
    <div class="login_div">
      <div class="login_heading">
        <h2>Login</h2>
      </div>
      <form action="" method="post"> 
        <table class="login_table">   
          <tr>   
            <div id="card_div">
              <td><h5>Card Number</h5></td>
              <td><input type="text" name="username"></td>
            </div>
          </tr>
          <tr>
            <div id="password_div">
              <td><h5>Password</h5></td>
              <td><input type="password" name="password"></td>
            </div> 
          </tr>
          <tr>
            <div id="button_div">
              <td></td>
              <td><input type="submit" value="Submit" name="submit"></td>
            </div>
          </tr>
        </table>
      </form>
    </div>
  </div>

<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>
