<!DOCTYPE html>
<html lang="en">
<head>
  <title>Mechatron | TechTatva'16</title>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/styles.css">

  <!-- JS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <script type="text/javascript" src="js/common.js"></script> 

  <script>

    function startTimer(duration, display_min, display_sec) 
    {
      var timer = duration, minutes, seconds;     
      setInterval(function () 
      {
          minutes = parseInt(timer / 60, 10)
          seconds = parseInt(timer % 60, 10);

          minutes = minutes < 10 ? "0" + minutes : minutes;
          seconds = seconds < 10 ? "0" + seconds : seconds;

          display_min.textContent = minutes;
          display_sec.textContent = seconds;

          if (--timer < 0) {
              window.open("buy_items.php","_self");
          }
      }, 1000);
    } 

    function timer(time) 
    {
      var fiveMinutes = 60 * time ;
      var display_min = document.querySelector("#minute");
      var display_sec = document.querySelector("#second");
      startTimer(fiveMinutes, display_min, display_sec);
    }
    </script>
</head>
<body>
  </div>
  <div id="page"> 
    <?php
      require_once("includes/header.php"); 
      require_once("dbcon.php");
      session_start();
    ?>

    <?php
      echo '<div id="clockdiv" style="margin-left: 87%;">
            <div>
              <span class="minutes" id="minute">00</span>
              <div class="smalltext">Minutes</div>
            </div>
            <div>
              <span class="seconds" id="second">00</span>
              <div class="smalltext">Seconds</div>
            </div>
          </div>';
    ?>

    <?php     
      $question = array();
      $choice1 = array();
      $choice2 = array();
      $choice3 = array();
      $answer = array();
      $img = array();
      $i = 0; 
      $query = "Select * from `questions`";
      $getDBTime="select timeStamp from login where userid=".$_SESSION["userid"];       
      $res=mysqli_query($connection,$getDBTime);
      while($DBTimeArray=mysqli_fetch_assoc($res))
      {
        $DBTime=$DBTimeArray["timeStamp"];
      }      
      $t=time();
      echo '<br>';
      $Time = 60-($t-$DBTime)/60;
      $Time = intval($Time);
      echo '<script>timer('.$Time.')</script><br>';
      $result = mysqli_query($connection, $query);
      if($result)
      {
        while($row = mysqli_fetch_assoc($result))
        {
          $question[$i] = $row["question"];
          $choice1[$i] = $row["choice1"];
          $choice2[$i] = $row["choice2"];
          $choice3[$i] = $row["choice3"];
          $answer[$i] = $row["answer"];
          $img[$i] = $row["url"];
          $i++;
        }
        $_SESSION['answer'] = $answer;
        $rows = $i;
        echo '<p id="number" style="display:none">'.$rows.'</p>';
        $order = array(0,1,2,3);
        shuffle($order);
        $orderChoices = array($choice1,$choice2,$choice3,$answer);
        $i = 0;
        $result = mysqli_query($connection, $query);
        echo '<form action="buy_items.php" method="post" id="question_form">';
        echo '<input type="submit" name="submit" value="Finish Test" class="finish_button" />';        
        $t=time();
        if($t - $DBTime >= 60*60)
          {
            echo "<script>window.open('gandmaro.html','_self');</script>";
          }
        if(isset($_POST["submit"]))
        {
          if($t - $DBTime >= 60*60)
          {
            echo "<script>window.open('instruction_page.php','_self');</script>";
          }
        }
        while($row = mysqli_fetch_assoc($result))
        {    
          if(isset($_POST["submit"]))
          {            
            break;
          }
          if($i == 0)
          {
            echo '<br>';
              echo '<div id="question_'.$i.'" class="question_div">
                      <p> 1. '.$question[$i].'</p><br>
                      <img src="'.$img[$i].'"><br><br>
                      <input type="radio" name="radio'.$i.'" value="'.$orderChoices[$order[0]][$i].'">  '.$orderChoices[$order[0]][$i].' <br><br>
                      <input type="radio" name="radio'.$i.'" value="'.$orderChoices[$order[1]][$i].'">  '.$orderChoices[$order[1]][$i].' <br><br>
                      <input type="radio" name="radio'.$i.'" value="'.$orderChoices[$order[2]][$i].'">  '.$orderChoices[$order[2]][$i].' <br><br>
                      <input type="radio" name="radio'.$i.'" value="'.$orderChoices[$order[3]][$i].'">  '.$orderChoices[$order[3]][$i].' <br><br>
                      <button id="next"'.$i.' type="button" onclick="goNext('.$i.') ">Next</button><br>
                    </div>';
          }
          else if($i > 0 && $i < ($rows - 1))
          {
            echo '<br>';
              echo '<div id="question_'.$i.'" class="hide">
                      <p> '.($i+1).'. '.$question[$i].'</p><br>
                      <img src="'.$img[$i].'"><br>
                      <input type="radio" name="radio'.$i.'" value="'.$orderChoices[$order[0]][$i].'">  '.$orderChoices[$order[0]][$i].' <br><br>
                      <input type="radio" name="radio'.$i.'" value="'.$orderChoices[$order[1]][$i].'">  '.$orderChoices[$order[1]][$i].' <br><br>
                      <input type="radio" name="radio'.$i.'" value="'.$orderChoices[$order[2]][$i].'">  '.$orderChoices[$order[2]][$i].' <br><br>
                      <input type="radio" name="radio'.$i.'" value="'.$orderChoices[$order[3]][$i].'">  '.$orderChoices[$order[3]][$i].' <br><br>
                      <button id="pre"'.$i.' type="button" onclick="goPrev('.$i.')">Previous</button>                  
                      <button id="next"'.$i.' type="button" onclick="goNext('.$i.')">Next</button><br>
                    </div>';
          }
          else
          {
            echo '<br>';
              echo '<div id="question_'.$i.'" class="hide">
                      <p> '.($i+1).'. '.$question[$i].'</p><br>
                      <img src="'.$img[$i].'"><br>
                      <input type="radio" name="radio'.$i.'" value="'.$orderChoices[$order[0]][$i].'">  '.$orderChoices[$order[0]][$i].' <br><br>
                      <input type="radio" name="radio'.$i.'" value="'.$orderChoices[$order[1]][$i].'">  '.$orderChoices[$order[1]][$i].' <br><br>
                      <input type="radio" name="radio'.$i.'" value="'.$orderChoices[$order[2]][$i].'">  '.$orderChoices[$order[2]][$i].' <br><br>
                      <input type="radio" name="radio'.$i.'" value="'.$orderChoices[$order[3]][$i].'">  '.$orderChoices[$order[3]][$i].' <br><br>
                      <button id="pre"'.$i.' type="button" onclick="goPrev('.$i.')">Previous</button>
                    </div>';
          }        
          $i++;
        }        
        echo '</form>';        
      }
      else
      {
        echo "Select query failed";
      }
      ?>
  </div>  
</body>
</html>